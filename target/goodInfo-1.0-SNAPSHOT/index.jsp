<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 11/8/2023
  Time: 下午4:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
<html lang="en">

<head>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
<style>
    /* 清除基本样式 */
    * {
        margin: 0;
        padding: 0;
    }

    body {
        /* 对整个body进行设置 */
        min-height: 100vh;
        /* 将图片置于背景的中心位置。背景图片不会被重复，并且固定在视窗中不随滚动条的滚动而移动 */
        background: #e7e7e7 url("/static/images/loginbg.jpeg") center no-repeat fixed;
        background-color: cover; /*设置背景的颜色，将背景的颜色设为尺寸覆盖整个视窗。*/
        background-size: cover;  /*设置背景图片的尺寸大小，使其随视窗大小的变化而自动调整，以覆盖整个背景区域。*/
        backdrop-filter: blur(2px); /* 设置背景的模糊效果*/
        /*弹性布局居中*/
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .container {
        background-color: #e7e7e7;
        border-radius: 0.7rem;
        /* 设置阴影效果，它的水平偏移量为 0，垂直偏移量为 0.9rem（即相对于元素的顶部距离），模糊半径为 1.7rem，颜色为 rgba(0, 0, 0, 0.25)。 */
        box-shadow:
                0 0.9rem 1.7rem rgba(0, 0, 0, 0.25) 0 0.7rem 1.7rem rgba(0, 0, 0, 0.22);
        height: 420px;
        max-width: 750px;
        overflow: hidden;
        position: relative;  /*父元素进行定位*/
        width: 100%;
    }

    .container-from {
        height: 100%;
        position: absolute; /*表单相对于container进行定位*/
        top: 0;
        transition: all 0.6s ease-in-out; /*设置动画效果适用于全部 0.6秒执行，平滑过渡*/
    }

    .container-signin {
        /* 登录部分整体设置 */
        left: 0;
        width: 50%;
        z-index: 2;
    }

    .container-signup {
        /* 注册部分 */
        left: 0;
        width: 50%;
        z-index: 1;
        opacity: 0;
        /* 透明不可见 */
    }

    .form {
        display: flex;
        align-items: center;
        justify-content: center;
        /*更改默认弹性盒主轴方向居中*/
        flex-direction: column;
        padding: 0 3rem;
        height: 100%;
        /* 文字居中 */
        text-align: center;
        background-color: #e7e7e7;
    }

    .form-title {
        /* 标题界面 */
        font-weight: 300;
        margin: 0;
        margin-bottom: 2rem;
    }

    .link {
        /* 忘记密码部分 */
        color: #333;
        font-size: 0.9rem;
        margin: 1.5rem 0;
        margin-bottom: 2px;
        /* 用于去除文本的装饰效果 */
        text-decoration: none;
    }

    .input {
        /* 对整体文本框的一个优化 */
        width: 100%;
        background-color: #fff;
        padding: 0.9rem 0.9rem;
        margin: 0.5rem 0;
        border: none;
        outline: none;
        border-radius: 6px;
    }

    .btn {
        /* 对整个按钮的样式美化 */
        background-color: #f25d8e;/*背景颜色，阴影，弧度，文字颜色，鼠标样式，文字样式*/
        box-shadow: 0 4px 4px rgba(255, 112, 159, 0.3);
        border-radius: 6px;
        color: #e7e7e7;
        cursor: pointer;
        font-size: 0.8rem;
        font-weight: bold;
        /* 属性设置字符间的间距为 0.1rem。这意味着文本中的字符将会相对于默认的间距稍微拉宽或缩小 */
        letter-spacing: 0.1rem;
        padding: 0.9rem 4rem;
        /* 关于缓动函数，ease-in 是其中一种，它在动画开始时具有较慢的加速度，逐渐加快。除了 ease-in，还有很多其他的缓动函数可供选择，如 ease-out（慢慢变慢）、ease-in-out（加速再减速）等。 */
        /* transition 属性指定了过渡效果的属性、持续时间和缓动函数。具体来说，transform 是过渡效果应用到的 CSS 属性，80ms 是过渡效果的持续时间，ease-in 是过渡效果的缓动函数。 */
        transition: transform 80ms ease-in;
    }
    /* .form > .btn 是一种 CSS 选择器，用于选择 .form 元素下直接子元素中的 .btn 元素 */
    .form>.btn {
        margin-top: 1.5rem;
    }
    /* btn被点击的时候，进行缩放动画 */
    .btn:active {
        transform: scale(0.8);
    }
    /* <!-- 叠层部分 --> */
    .container-overlay {
        height: 100%;
        left: 50%;
        /* 隐藏掉超出部分的图画 */
        overflow: hidden;
        position: absolute;
        top: 0;
        transition: transform 0.6s ease-in-out;
        width: 50%;
        /* 优先显示级别最高 遮住空白处，使得显示动画 */
        z-index: 100;
    }

    .overlay {
        /* 父容器的 200% 100%  */
        width: 200%;
        height: 100%;
        /* 的定位设置为相对定位，使得后续的定位属性可以生效。 */
        position: relative;
        /* 左边缘定位到父容器的左边缘外，即向左移动一个父容器的宽度。 */
        left: -100%;
        /* 再次引入背景图 */
        background: url("/static/images/loginbg.jpeg")no-repeat center fixed;
        background-size: cover;
        transition: transform 0.6s ease-in-out;
        transform: translateX(0);
        /* 将 .overlay 进行水平方向平移，当前设置为 translateX(0) 表示不发生平移。 */
    }

    .overlay-panel {
        /* 控制按钮有无账号那一部分 */
        /* 与父元素同高则定位居中 */
        height: 100%;
        width: 50%;
        /* 相对于overlay绝对定位 */
        position: absolute;
        /* 弹性盒子水平垂直居中切变换主轴为y轴 */
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        /* transform: translateX(0); */
        transition: transform 0.6s ease-in-out;
    }

    .overlay-left {
        /* 左边部分,已有账号直接登录,x轴平移-20% */
        transform: translateX(-20%);
    }

    .overlay-right {
        /* 右部分,没有账号按钮 */
        right: -3px;
        transform: translateX(0);
    }
    /* 重点在这!! 通过添加或移除 panel-active 类，切换面板的状态，触发相应的样式变化，从而实现了页面切换的动画效果。*/
    /*这段代码定义了在应用 .panel-active 类时，不同元素的动画效果。通过改变 transform 属性的值，
    可以控制各个元素的水平平移效果，从而实现动画过渡效果，将元素由隐藏状态移动到可见位置。同时，还改变了透明度和堆叠顺序，以控制元素的可见性和显示层级。*/
    /* 定义了这样一个动画,并且在后面js部分通过点击实现了左右移动,添加样式达到实现动画的情况 */
    /* 与下面js代码结合在一起形成了一个切面效果 当点击 signInBtn 按钮时，执行的函数会移除 container 元素的 panel-active 类，使得切换面板回到原始状态。
    当点击 signUpBtn 按钮时，执行的函数会为 container 元素添加 panel-active 类，触发面板的切换效果。*/
    /* 当 container 具有 panel-active 类时，.panel-active .overlay-left 的样式规则将使 .overlay-left 元素在水平方向上不发生平移；
    同样地，.panel-active .container-overlay 规则将使 .container-overlay 元素向左平移它的宽度的 100%；
    .panel-active .overlay 规则将使 .overlay 元素向右平移自身宽度的 50%；
    .panel-active .container-signin 规则将使 .container-signin 元素向右平移自身宽度的 100%；
    .panel-active .container-signup 规则将使 .container-signup 元素向右平移自身宽度的 100%，并设置其不透明度为 1，堆叠顺序为 5。 */
    .panel-active .overlay-left {
        transform: translateX(0);
    }

    .panel-active .container-overlay {
        transform: translateX(-100%);
    }

    .panel-active .overlay {
        transform: translateX(50%);
    }

    .panel-active .container-signin {
        transform: translateX(100%);
    }

    .panel-active .container-signup {
        opacity: 1;
        z-index: 5;
        transform: translateX(100%);
    }
</style>

<div class="container">
    <!-- 注册部分 -->
    <div class="container-from container-signup">
        <form  class="form" id="from1">
            <h2 class="form-title">注册账号</h2>
            <input type="text" placeholder="User" class="input" />
            <input type="email" placeholder="Email" class="input" />
            <input type="password" placeholder="Password" class="input">
            <button type="button" class="btn">点击注册</button>
        </form>
    </div>
    <!-- 登录 -->
    <div class="container-from container-signin">
        <form method="post" action="http://localhost:8080/show" class="form" id="from2" >
            <h2 class="form-title">欢迎登录</h2>
            <input type="text" placeholder="Username" class="input" name="username"/>
            <input type="password" placeholder="Password" class="input" name="password">
            <a href="" class="link">忘记密码</a>
            <button type="" class="btn">登录</button>
        </form>
    </div>

    <!-- 叠层部分 -->
    <div class="container-overlay">
        <div class="overlay">
            <div class="overlay-panel overlay-left">
                <button class="btn" id="signIn">已有账号，直接登录</button>
            </div>
            <div class="overlay-panel overlay-right">
                <button class="btn" id="signUp">没有账号点击注册
                </button>
            </div>
        </div>
    </div>
</div>
</body>
<script>
    // 段代码为选取的两个按钮绑定了点击事件，并在按钮点击时，通过添加或移除特定的类名（panel-active），
    //来实现容器元素的样式变化效果。当点击 “signIn” 按钮时，移除 panel-active 类，将容器恢复到非活动状态；
    //当点击 “signUp” 按钮时，添加 panel-active 类，将容器变为活动状态。这些类名的添加或移除会触发关联的 CSS 样式规则
    //从而实现相应元素的动画过渡效果。
    const signInBtn = document.querySelector('#signIn');
    const signUpBtn = document.querySelector('#signUp')
    console.log(signUpBtn)
    const container = document.querySelector('.container')
    signInBtn.onclick = function () {
        container.classList.remove('panel-active')
    }
    signUpBtn.onclick = function () {
        container.classList.add('panel-active')
    }
</script>

</html>
