<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 11/8/2023
  Time: 下午6:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>数据可视化项目</title>
    <link rel="stylesheet" href="/static/fonts/icomoon.css" />
    <link rel="stylesheet" href="/static/css/index.css" />
    <script src="/static/js/echarts.min.js"></script>
    <script src="/static/js/flexible.js"></script>
    <script src="/static/js/jquery.min.js"></script>
</head>

<body>

<div class="viewport">
    <div class="column">
        <div class="panel overview">
            <div class="inner">
                <ul>
                    <li>

                        <h4>${requestScope.oneInfo.totalNum}</h4>
                        <span>
                  <i class="icon-dot" style="color: #006cff"></i>
                  今日订单总数
                </span>
                    </li>
                    <li class="item">
                        <h4>${requestScope.oneInfo.newAdd}</h4>
                        <span>
                  <i class="icon-dot" style="color: #6acca3"></i>
                  今日新增
                </span>
                    </li>
                    <li>
                        <h4>${requestScope.oneInfo.userClick}</h4>
                        <span>
                  <i class="icon-dot" style="color: #6acca3"></i>
                  用户点击量
                </span>
                    </li>

                </ul>
            </div>
        </div>

        <div class="panel monitor">
            <div class="inner">
                <div class="tabs">
                    <a href="javascript:;" class="active">订单信息</a>
                </div>
                <div class="content" style="display: block;">
                    <div class="head">
                        <span class="col">订单编号</span>
                        <span class="col">创建时间</span>
                        <span class="col">收货地址</span>
                    </div>
                    <div class="marquee-view">
                        <div class="marquee">
                            <c:forEach items="${requestScope.all}" var="info">
                                <div class="row">
                                    <span class="col">00000${info.goodIndex}</span>
                                    <span class="col">${info.goodTime}</span>
                                    <span class="col">${info.goodPlace}</span>
                                    <span class="icon-dot"></span>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- 模块2 -->
        <div class="point panel">
            <div class="inner">

                <h3>区域分布统计</h3>
                <div class="chart">
                    <div class="pie">
                        <script>
                              var myChart = echarts.init(document.querySelector(".pie"));
                              // 2. 指定配置项和数据
                              var option = {
                                tooltip: {
                                  trigger: "item",
                                  formatter: "{a} <br/>{b} : {c} ({d}%)"
                                },
                                // 注意颜色写的位置
                                color: [
                                  "#006cff",
                                  "#60cda0",
                                  "#ed8884",
                                  "#ff9f7f",
                                  "#0096ff",
                                  "#9fe6b8",
                                  "#32c5e9",
                                  "#1d9dff"
                                ],
                                series: [
                                  {
                                    name: "点位统计",
                                    type: "pie",
                                    radius: ["10%", "70%"],
                                    center: ["50%", "50%"],
                                    roseType: "radius",
                                    data: [
                                    <c:forEach items="${requestScope.allR}" var="region">
                                      { value: ${region.count}, name: "${region.place}" },
                                        </c:forEach>
                                    ],
                                    label: {
                                      fontSize: 10
                                    },
                                    labelLine: {
                                      length: 6,
                                      length2: 8
                                    }
                                  }
                                ]
                              };
                            myChart.setOption(option);
                              window.addEventListener("resize", function() {
                                myChart.resize();
                              })
                        </script>
                    </div>
                    <div class="data">
                        <div class="item">
                            <h4>0</h4>
                            <span>
                    <i class="icon-dot" style="color: #ed3f35"></i>
                    位置总数
                  </span>
                        </div>
                        <div class="item">
                            <h4>0</h4>
                            <span>
                    <i class="icon-dot" style="color: #eacf19"></i>
                    本月新增
                  </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="column">
        <!-- 模块3 -->
        <div class="map">
            <h3>
                <span class="icon-cube"></span>
                物流数据统计
            </h3>
            <div class="chart">
                <div class="geo"></div>
            </div>
        </div>
        <!-- 模块0 -->
        <div class="users panel">
            <div class="inner">
                <h3>Ⅰ类订单分布统计</h3>
                <div class="chart">
                    <div class="bar">
                        <script>
                            var item = {
                                name: "",
                                value: 1200,
                                itemStyle: {
                                    color: "#254065"
                                },
                                emphasis: {
                                    itemStyle: {
                                        color: "#254065"
                                    }
                                },
                                tooltip: {
                                    extraCssText: "opacity: 0"
                                }
                            };
                            var myEchart = echarts.init(document.querySelector(".bar"));

                            var option = {
                                color: new echarts.graphic.LinearGradient(
                                    0,
                                    0,
                                    0,
                                    1,
                                    [
                                        { offset: 0, color: "#00fffb" },
                                        { offset: 1, color: "#0061ce" }
                                    ]
                                ),
                                tooltip: {
                                    trigger: "item"
                                },
                                grid: {
                                    left: "0%",
                                    right: "3%",
                                    bottom: "3%",
                                    top: "3%",
                                    containLabel: true,
                                    show: true,
                                    borderColor: "rgba(0, 240, 255, 0.3)"
                                },
                                xAxis: [
                                    {
                                        type: "category",
                                        data: [
                                            <c:forEach items="${requestScope.userPs}" var="userP">
                                            "${userP.userPlace}",</c:forEach>

                                        ],
                                        axisTick: {
                                            alignWithLabel: false,
                                            show: false
                                        },
                                        axisLabel: {
                                            color: "#4c9bfd"
                                        },
                                        axisLine: {
                                            lineStyle: {
                                                color: "rgba(0, 240, 255, 0.3)"
                                            }
                                        }
                                    }
                                ],
                                yAxis: [
                                    {
                                        type: "value",
                                        axisTick: {
                                            alignWithLabel: false,
                                            show: false
                                        },
                                        axisLabel: {
                                            color: "#4c9bfd"
                                        },
                                        axisLine: {
                                            lineStyle: {
                                                color: "rgba(0, 240, 255, 0.3)"
                                            }
                                        },
                                        splitLine: {
                                            lineStyle: {
                                                color: "rgba(0, 240, 255, 0.3)"
                                            }
                                        }
                                    }
                                ],
                                series: [
                                    {
                                        name: "完成支付",
                                        type: "bar",
                                        barWidth: "60%",
                                        data: [
                                            <c:forEach items="${requestScope.userPs}" var="userp">
                                            ${userp.userNum},
                                            </c:forEach>

                                        ]
                                    }
                                ]
                            };
                            myEchart.setOption(option);
                            window.addEventListener("resize", function() {
                                myChart.resize();
                            })
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="column">
        <!-- 订单 -->
        <div class="order panel">
            <div class="inner">
                <!-- 筛选 -->
                <div class="filter">
                    <a href="javascript:;" class="active">365天</a>
                    <a href="javascript:;">90天</a>
                    <a href="javascript:;">30天</a>
                    <a href="javascript:;">24小时</a>
                </div>
                <!-- 数据 -->
                <div class="data">
                    <div class="item">
                        <h4>${requestScope.total.totalSum}</h4>
                        <span>
                  <i class="icon-dot" style="color: #ed3f35;"></i>
                  订单量
                </span>
                    </div>
                    <div class="item">
                        <h4>${requestScope.total.totalAmount}</h4>
                        <span>
                  <i class="icon-dot" style="color: #eacf19;"></i>
                  销售额(万元)
                </span>
                    </div>
                </div>
            </div>
        </div>
        <!-- 销售额 -->
        <div class="sales panel">
            <div class="inner">
                <div class="caption">
                    <h3>销售额统计</h3>
                    <a href="javascript:;" class="activeY" data-type="year">年</a>
                    <a href="javascript:;" data-type="quarter">季</a>
                    <a href="javascript:;" data-type="month">月</a>
                    <a href="javascript:;" data-type="week">周</a>
                </div>

                <div class="chart">
                    <div class="label">单位:万</div>
                    <div class="line">

                        <script>
                        var data = {
                            year: [

                                [<c:forEach items="${requestScope.sales}" var="sale">
                                    ${sale.yearExpect},
                                    </c:forEach>],
                                [<c:forEach items="${requestScope.sales}" var="sale">
                                    ${sale.yearReality},
                                    </c:forEach>]
                            ],
                            quarter: [
                                [23, 75, 12, 97, 21, 67, 98, 21, 43, 64, 76, 38],
                                [43, 31, 65, 23, 78, 21, 82, 64, 43, 60, 19, 34]
                            ],
                            month: [
                                [34, 87, 32, 76, 98, 12, 32, 87, 39, 36, 29, 36],
                                [56, 43, 98, 21, 56, 87, 43, 12, 43, 54, 12, 98]
                            ],
                            week: [
                                [43, 73, 62, 54, 91, 54, 84, 43, 86, 43, 54, 53],
                                [32, 54, 34, 87, 32, 45, 62, 68, 93, 54, 54, 24]
                            ]
                        };
                        var myEchart = echarts.init(document.querySelector(".line"));

                        var option = {
                            color: ["#00f2f1", "#ed3f35"],
                            tooltip: {
                                trigger: "axis"
                            },
                            legend: {
                                right: "10%",
                                textStyle: {
                                    color: "#4c9bfd"
                                }
                            },
                            grid: {
                                top: "20%",
                                left: "3%",
                                right: "4%",
                                bottom: "3%",
                                show: true,
                                borderColor: "#012f4a",
                                containLabel: true
                            },

                            xAxis: {
                                type: "category",
                                boundaryGap: false,
                                data: [<c:forEach items="${requestScope.sales}" var="sale">
                                    "${sale.month}",
                                    </c:forEach>

                                ],
                                axisTick: {
                                    show: false
                                },
                                axisLabel: {
                                    color: "#4c9bfd"
                                },
                                axisLine: {
                                    show: false
                                }
                            },
                            yAxis: {
                                type: "value",
                                axisTick: {
                                    show: false
                                },
                                axisLabel: {
                                    color: "#4c9bfd"
                                },
                                splitLine: {
                                    lineStyle: {
                                        color: "#012f4a"
                                    }
                                }
                            },
                            series: [
                                {
                                    name: "预期销售额",
                                    type: "line",
                                    stack: "总量",
                                    smooth: true,
                                    data: data.year[0]
                                },
                                {
                                    name: "实际销售额",
                                    type: "line",
                                    stack: "总量",
                                    smooth: true,
                                    data: data.year[1]
                                }
                            ]
                        };
                        myEchart.setOption(option);

                        window.addEventListener("resize", function() {
                            myChart.resize();
                        });
                    </script>

                    </div>
                </div>
            </div>
        </div>
        <!-- 模块4 -->
        <div class="wrap">
            <div class="channel panel">
                <div class="inner">
                    <h3>品类分布</h3>
                    <div class="data">
                        <div class="radar"></div>
                    </div>
                </div>
            </div>
            <!-- 模块5 -->
            <div class="quarter panel">
                <div class="inner">
                    <h3>销售进度</h3>
                    <div class="chart">
                        <div class="box">
                            <div class="gauge"></div>
                            <div class="label">66<small> %</small></div>
                        </div>
                        <div class="data">
                            <div class="item">
                                <h4>6,666</h4>
                                <span>
                      <i class="icon-dot" style="color: #6acca3"></i>
                      销售额(万元)
                    </span>
                            </div>
                            <div class="item">
                                <h4>50%</h4>
                                <span>
                      <i class="icon-dot" style="color: #ed3f35"></i>
                      同比增长
                    </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- 未完成 -->
        <div class="top panel">
            <div class="inner" style="
                          position: absolute;
                          top: -0.5rem;
                          left: -0.6rem;
                          right: -0.6rem;
                          bottom: -0.2rem;
                          padding: 0.2rem 0.1rem;">
                <h3>下单时间分布趋势</h3>
                <div class="chart2">
                    <div class="time">
                        <script>
                            var myEchart = echarts.init(document.querySelector(".time"));

                            option = {
                                tooltip: {
                                    trigger: 'item'
                                },
                                legend: {
                                    top: '5%',
                                    left: 'center'
                                },
                                series: [
                                    {
                                        name: '下单量',
                                        type: 'pie',
                                        radius: ['42%', '70%'],
                                        avoidLabelOverlap: false,
                                        itemStyle: {
                                            borderRadius: 10,
                                            borderColor: '#fff',
                                            borderWidth: 2
                                        },
                                        label: {
                                            show: false,
                                            position: 'center'
                                        },
                                        emphasis: {
                                            label: {
                                                show: true,
                                                fontSize: 14,
                                                fontWeight: 'bold'
                                            }
                                        },
                                        labelLine: {
                                            show: false
                                        },
                                        data: [
                                            <c:forEach items="${requestScope.allT}" var="timeone">
                                            { value: ${timeone.count}, name: '${timeone.timer}' },
                                            </c:forEach>

                                        ]
                                    }
                                ]
                            };
                            myEchart.setOption(option);

                            window.addEventListener("resize", function() {
                                myChart.resize();
                            });
                        </script>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script src="/static/js/index.js"></script>
<script src="/static/js/china.js"></script>
<script src="/static/js/myMap.js"></script>
</body>
</html>
