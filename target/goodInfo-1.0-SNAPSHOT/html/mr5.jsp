<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 12/8/2023
  Time: 下午5:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<div id="box" style="width: 600px; height: 600px"></div>
<script src="/static/js/echarts.min.js"></script>

<script>
    var myEchart = echarts.init(document.getElementById("box"));

    option = {
        tooltip: {
            trigger: 'item'
        },
        legend: {
            top: '5%',
            left: 'center'
        },
        series: [
            {
                name: '下单量',
                type: 'pie',
                radius: ['42%', '70%'],
                avoidLabelOverlap: false,
                itemStyle: {
                    borderRadius: 10,
                    borderColor: '#fff',
                    borderWidth: 2
                },
                label: {
                    show: false,
                    position: 'center'
                },
                emphasis: {
                    label: {
                        show: true,
                        fontSize: 14,
                        fontWeight: 'bold'
                    }
                },
                labelLine: {
                    show: false
                },
                data: [
                    <c:forEach items="${requestScope.all1}" var="tone">
                    { value: ${tone.count}, name: '${tone.timer}' },
                    </c:forEach>

                ]
            }
        ]
    };
    myEchart.setOption(option);

    window.addEventListener("resize", function() {
        myChart.resize();
    });
</script>
</body>
</html>
