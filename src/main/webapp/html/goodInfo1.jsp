<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 12/8/2023
  Time: 上午8:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>订单信息表1</h1>
<hr>
<table>
    <thead>
    <th>订单编号</th>
    <th>订单时间</th>
    <th>订单地点</th>
    </thead>
    <tbody>
    <c:forEach items="${requestScope.all}" var="info">
        <tr>
            <td>00000${info.goodIndex}</td>
            <td>${info.goodTime}</td>
            <td>${info.goodPlace}</td>
        </tr>
    </c:forEach>

    </tbody>
</table>
</body>
</html>
