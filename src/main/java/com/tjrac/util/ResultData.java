package com.tjrac.util;

/**
 * --- 天道酬勤 ---
 *
 * @author QiuShiju
 * @desc 封装的结果数据
 * --> 符合layui默认模板格式
 * {
 *   "code": 0,
 *   "msg": "",
 *   "count": 1000,
 *   "data": [{}, {}]
 * }
 */
public class ResultData {

    private int code; // 0是成功,其他都是不成功
    private String msg;
    private long count;
    private Object data;

    public ResultData() {
    }

    public static ResultData ok() {
        ResultData resultData = new ResultData( );
        resultData.setCode(0);
        resultData.setMsg("成功");
        return resultData;
    }

    public static ResultData ok(Object data) {
        ResultData resultData = new ResultData( );
        resultData.setCode(0);
        resultData.setMsg("成功");
        resultData.setData(data);

        return resultData;
    }

    public static ResultData ok(Object data,long count) {
        ResultData resultData = new ResultData( );
        resultData.setCode(0);
        resultData.setMsg("成功");
        resultData.setData(data);
        resultData.setCount(count);
        return resultData;
    }

    public static ResultData fail() {
        ResultData resultData = new ResultData( );
        resultData.setCode(-1);
        resultData.setMsg("失败");
        return resultData;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
