package com.tjrac.mr;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import java.io.IOException;
import java.util.List;

/**
 * @author Lenovo
 * @version 1.0
 * @description TODO
 * @date 12/8/2023 上午10:33
 */

public class TimeMr {
    public static class MyMapper extends Mapper<LongWritable, Text,Text, IntWritable> {
        private static  final IntWritable timeS = new IntWritable();
        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String line = value.toString();
            String[] times = line.split("\\s+");
            for (String time : times) {
                context.write(new Text(times[4]),timeS);
            }
        }
    }
    //reduce 输入key value
    public static  class MyReducer extends Reducer<Text, IntWritable,Text, LongWritable> {

        @Override
        protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
            int count1 = 0;
            int count2 = 0;
            int count3 = 0;
            int count4 = 0;
            int count5 = 0;
            for (IntWritable value : values) {
                if (value.get()<24){
                    count1++;
                } else if (value.get()<18) {
                    count2++;
                } else if (value.get()<12) {
                    count3++;
                } else if (value.get()<6) {
                    count4++;
                }else {
                    count5++;
                }



            }
            context.write(key,new LongWritable(count1));
        }
    }


    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();
        conf.set("mapred.job.tracker", "192.168.100.120:9001");
        conf.set("fs.default.name", "hdfs://192.168.100.120:8020");
        Job job = Job.getInstance(conf, "totalAmount");
        job.setJarByClass(totalAmount.class);
        FileInputFormat.setInputPaths(job,new Path("/goodInfo/*"));
        job.setInputFormatClass(TextInputFormat.class);
        job.setMapperClass(totalAmount.MyMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);
        job.setNumReduceTasks(1);
        job.setReducerClass(totalAmount.MyReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(LongWritable.class);
        FileOutputFormat.setOutputPath(job,new Path("/RegionOut/"));
        job.setOutputFormatClass(TextOutputFormat.class);
        boolean result = job.waitForCompletion(true);
        System.exit(result ? 0 : 1);
    }
}
