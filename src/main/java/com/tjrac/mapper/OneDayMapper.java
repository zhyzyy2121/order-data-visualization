package com.tjrac.mapper;

import com.tjrac.entities.OneDay;

/**
 * @author Lenovo
 * @version 1.0
 * @description TODO
 * @date 11/8/2023 下午6:07
 */

public interface OneDayMapper {
   OneDay findOneInfo(int status);
}