package com.tjrac.mapper;

import com.tjrac.entities.Admin;
import org.apache.ibatis.annotations.Param;

/**
 * @author Lenovo
 * @version 1.0
 * @description TODO
 * @date 11/8/2023 下午4:04
 */

public interface AdminMapper {
    Admin findUserByLogin(@Param("username") String userName, @Param("password") String userPassword);

}