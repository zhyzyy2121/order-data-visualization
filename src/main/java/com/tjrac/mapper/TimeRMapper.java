package com.tjrac.mapper;

import com.tjrac.entities.TimeR;

import java.util.List;

/**
 * @author Lenovo
 * @version 1.0
 * @description TODO
 * @date 4/9/2023 上午7:35
 */

public interface TimeRMapper {
    List<TimeR> findAll();
}