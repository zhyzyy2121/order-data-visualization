package com.tjrac.mapper;

import com.tjrac.entities.GoodInfo;

import java.util.List;

/**
 * @author Lenovo
 * @version 1.0
 * @description TODO
 * @date 12/8/2023 上午8:50
 */

public interface GoodInfoMapper {
    List<GoodInfo> findAll();
}