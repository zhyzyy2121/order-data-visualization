package com.tjrac.mapper;

import com.tjrac.entities.SalesAll;

import java.util.List;

/**
 * @author Lenovo
 * @version 1.0
 * @description TODO
 * @date 12/8/2023 下午5:13
 */

public interface SalesMapper {
    List<SalesAll> findAll();
}