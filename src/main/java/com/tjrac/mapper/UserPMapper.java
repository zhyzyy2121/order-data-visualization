package com.tjrac.mapper;

import com.tjrac.entities.UserP;

import java.util.List;

/**
 * @author Lenovo
 * @version 1.0
 * @description TODO
 * @date 12/8/2023 下午4:16
 */

public interface UserPMapper {
    List<UserP> findAll();
}