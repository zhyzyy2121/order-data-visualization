package com.tjrac.mapper;

import com.tjrac.entities.Region;

import java.util.List;

/**
 * @author Lenovo
 * @version 1.0
 * @description TODO
 * @date 12/8/2023 上午10:44
 */

public interface RegionMapper {
    List<Region> findAll();
}