package com.tjrac.mapper;

import com.tjrac.entities.Total;

/**
 * @author Lenovo
 * @version 1.0
 * @description TODO
 * @date 12/8/2023 上午9:54
 */

public interface TotalMapper {
    Total findOne();
}