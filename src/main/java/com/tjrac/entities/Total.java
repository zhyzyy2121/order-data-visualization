package com.tjrac.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapred.lib.db.DBWritable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Lenovo
 * @version 1.0
 * @description TODO
 * @date 12/8/2023 上午9:47
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Total implements Writable, Serializable, DBWritable {
    private String totalAmount;
    private String totalSum;
    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeUTF(this.totalAmount);
        dataOutput.writeUTF(this.totalSum);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        this.totalAmount =dataInput.readUTF();
        this.totalSum=dataInput.readUTF();
    }

    @Override
    public void write(PreparedStatement ps) throws SQLException {
        ps.setString(1,this.totalAmount);
        ps.setString(2,this.totalSum);
    }

    @Override
    public void readFields(ResultSet re) throws SQLException {
        this.totalAmount =re.getString("totalAmount");
        this.totalSum=re.getString("totalSum");
    }
}
