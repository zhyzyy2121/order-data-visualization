package com.tjrac.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author Lenovo
 * @version 1.0
 * @description TODO
 * @date 11/8/2023 下午4:02
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Admin implements Serializable {
    private String username;
    private String password;
}
