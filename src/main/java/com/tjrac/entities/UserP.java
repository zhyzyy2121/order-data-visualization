package com.tjrac.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Lenovo
 * @version 1.0
 * @description TODO
 * @date 12/8/2023 下午4:15
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserP {
    private String userPlace;
    private String userNum;

}
