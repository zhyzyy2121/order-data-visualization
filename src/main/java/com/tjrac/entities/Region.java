package com.tjrac.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapred.lib.db.DBWritable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Lenovo
 * @version 1.0
 * @description TODO
 * @date 12/8/2023 上午10:39
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Region implements Writable, Serializable, DBWritable {
    private String count;
    private String place;

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeUTF(this.count);
        dataOutput.writeUTF(this.place);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        this.count =dataInput.readUTF();
        this.place=dataInput.readUTF();
    }

    @Override
    public void write(PreparedStatement ps) throws SQLException {
        ps.setString(1,this.count);
        ps.setString(2,this.place);
    }

    @Override
    public void readFields(ResultSet re) throws SQLException {
        this.count =re.getString("count");
        this.place=re.getString("place");
    }
}
