package com.tjrac.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author Lenovo
 * @version 1.0
 * @description TODO
 * @date 11/8/2023 下午6:04
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OneDay implements Serializable {
    private String totalNum;
    private String newAdd;
    private String userClick;
}
