package com.tjrac.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Lenovo
 * @version 1.0
 * @description TODO
 * @date 12/8/2023 上午8:35
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GoodInfo {
    private Integer goodIndex;
    private String goodTime;
    private String goodPlace;
}
