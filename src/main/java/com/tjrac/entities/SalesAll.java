package com.tjrac.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Lenovo
 * @version 1.0
 * @description TODO
 * @date 12/8/2023 下午5:03
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SalesAll {
    private String month;
    private String yearExpect;
    private String quarterExpect;
    private String monthExpect;
    private String weekExpect;
    private String yearReality;
    private String quarterReality;
    private String monthReality;
    private String weekReality;


}
