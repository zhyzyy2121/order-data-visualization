package com.tjrac.entities;

import lombok.Data;

/**
 * @author Lenovo
 * @version 1.0
 * @description TODO
 * @date 4/9/2023 上午7:33
 */
@Data
public class TimeR {
    private String timer;
    private Integer count;
}
