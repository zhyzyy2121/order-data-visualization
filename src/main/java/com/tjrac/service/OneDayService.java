package com.tjrac.service;

import com.tjrac.entities.OneDay;

/**
 * @author Lenovo
 * @version 1.0
 * @description TODO
 * @date 11/8/2023 下午6:16
 */

public interface OneDayService {
    OneDay findOneInfo(int status);
}