package com.tjrac.service;

import com.tjrac.entities.SalesAll;

import java.util.List;

/**
 * @author Lenovo
 * @version 1.0
 * @description TODO
 * @date 12/8/2023 下午5:15
 */

public interface SalesService {
    List<SalesAll> findAll();
}