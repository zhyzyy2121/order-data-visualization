package com.tjrac.service;

import com.tjrac.entities.GoodInfo;

import java.util.List;

/**
 * @author Lenovo
 * @version 1.0
 * @description TODO
 * @date 12/8/2023 上午8:52
 */

public interface GoodInfoService {
    List<GoodInfo> findAll();
}