package com.tjrac.service.Impl;

import com.tjrac.entities.UserP;
import com.tjrac.mapper.UserPMapper;
import com.tjrac.service.UserPService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Lenovo
 * @version 1.0
 * @description TODO
 * @date 12/8/2023 下午4:20
 */
@Service
public class UserPServiceImpl implements UserPService {
    @Autowired
    private UserPMapper userPMapper;

    @Override
    public List<UserP> findAll() {
        return userPMapper.findAll();
    }
}
