package com.tjrac.service.Impl;

import com.tjrac.entities.Total;
import com.tjrac.mapper.TotalMapper;
import com.tjrac.service.TotalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Lenovo
 * @version 1.0
 * @description TODO
 * @date 12/8/2023 上午9:56
 */
@Service
public class TotalServiceImpl implements TotalService {
    @Autowired
    private TotalMapper totalMapper;
    @Override
    public Total findOne() {
        return totalMapper.findOne();
    }
}
