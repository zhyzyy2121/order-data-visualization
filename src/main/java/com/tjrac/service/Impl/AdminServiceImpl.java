package com.tjrac.service.Impl;

import com.tjrac.entities.Admin;
import com.tjrac.mapper.AdminMapper;
import com.tjrac.service.AdminService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Lenovo
 * @version 1.0
 * @description TODO
 * @date 11/8/2023 下午4:11
 */
@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    private AdminMapper adminMapper;
    public Admin findUserByLogin(@Param("username") String userName, @Param("password") String userPassword){
        return adminMapper.findUserByLogin(userName,userPassword);
    }
}
