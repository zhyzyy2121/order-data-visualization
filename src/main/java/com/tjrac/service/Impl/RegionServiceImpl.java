package com.tjrac.service.Impl;

import com.tjrac.entities.Region;
import com.tjrac.mapper.RegionMapper;
import com.tjrac.service.RegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Lenovo
 * @version 1.0
 * @description TODO
 * @date 12/8/2023 上午10:46
 */
@Service
public class RegionServiceImpl implements RegionService {
    @Autowired
    private RegionMapper regionMapper;

    @Override
    public List<Region> findAll() {
        return regionMapper.findAll();
    }
}
