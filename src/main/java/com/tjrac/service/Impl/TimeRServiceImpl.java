package com.tjrac.service.Impl;

import com.tjrac.entities.TimeR;
import com.tjrac.entities.Total;
import com.tjrac.mapper.TimeRMapper;
import com.tjrac.mapper.TotalMapper;
import com.tjrac.service.TimeRService;
import com.tjrac.service.TotalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Lenovo
 * @version 1.0
 * @description TODO
 * @date 12/8/2023 上午9:56
 */
@Service
public class TimeRServiceImpl implements TimeRService {

    @Autowired
    private TimeRMapper timeRMapper;
    @Override
    public List<TimeR> findAll() {
        return timeRMapper.findAll();
    }
}
