package com.tjrac.service.Impl;

import com.tjrac.entities.SalesAll;
import com.tjrac.mapper.SalesMapper;
import com.tjrac.service.SalesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Lenovo
 * @version 1.0
 * @description TODO
 * @date 12/8/2023 下午5:16
 */
@Service
public class SalesServiceImpl implements SalesService {
    @Autowired
    private SalesMapper salesMapper;
    @Override
    public List<SalesAll> findAll() {
        return salesMapper.findAll();
    }
}
