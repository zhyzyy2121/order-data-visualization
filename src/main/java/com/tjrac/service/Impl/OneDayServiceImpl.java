package com.tjrac.service.Impl;

import com.tjrac.entities.OneDay;
import com.tjrac.mapper.OneDayMapper;
import com.tjrac.service.OneDayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Lenovo
 * @version 1.0
 * @description TODO
 * @date 11/8/2023 下午6:16
 */
@Service
public class OneDayServiceImpl implements OneDayService {
    @Autowired
    private OneDayMapper oneDayMapper;

    @Override
    public OneDay findOneInfo(int status) {
        return oneDayMapper.findOneInfo(status);
    }
}
