package com.tjrac.service.Impl;

import com.tjrac.entities.GoodInfo;
import com.tjrac.mapper.GoodInfoMapper;
import com.tjrac.service.GoodInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Lenovo
 * @version 1.0
 * @description TODO
 * @date 12/8/2023 上午8:52
 */
@Service
public class GoodInfoServiceImpl implements GoodInfoService {
    @Autowired
    private GoodInfoMapper goodInfoMapper;
    @Override
    public List<GoodInfo> findAll() {
        return goodInfoMapper.findAll();
    }
}
