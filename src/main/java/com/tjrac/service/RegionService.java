package com.tjrac.service;

import com.tjrac.entities.Region;

import java.util.List;

/**
 * @author Lenovo
 * @version 1.0
 * @description TODO
 * @date 12/8/2023 上午10:46
 */

public interface RegionService {
    List<Region> findAll();
}