package com.tjrac.service;

import com.tjrac.entities.UserP;

import java.util.List;

/**
 * @author Lenovo
 * @version 1.0
 * @description TODO
 * @date 12/8/2023 下午4:19
 */

public interface UserPService {
    List<UserP> findAll();
}