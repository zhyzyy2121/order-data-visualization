package com.tjrac.service;

import com.tjrac.entities.Admin;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

/**
 * @author Lenovo
 * @version 1.0
 * @description TODO
 * @date 11/8/2023 下午4:10
 */

public interface AdminService {

     Admin findUserByLogin(@Param("username") String userName, @Param("password") String userPassword);
}
