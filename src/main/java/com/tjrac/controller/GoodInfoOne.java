package com.tjrac.controller;

import com.tjrac.entities.GoodInfo;
import com.tjrac.service.GoodInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Lenovo
 * @version 1.0
 * @description TODO
 * @date 12/8/2023 上午8:54
 */
@Controller
public class GoodInfoOne {
    @Autowired
    private GoodInfoService goodInfoService;
    @RequestMapping("/goodInfo")
    public String findAll(HttpServletRequest req){
        List<GoodInfo> all = goodInfoService.findAll();
        req.setAttribute("all",all);
        return "goodInfo1";
    }

}
