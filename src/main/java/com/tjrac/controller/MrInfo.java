package com.tjrac.controller;

import com.tjrac.entities.*;
import com.tjrac.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Lenovo
 * @version 1.0
 * @description TODO
 * @date 12/8/2023 上午9:57
 */
@Controller
public class MrInfo {
    @Autowired
    private TotalService totalService;
    @Autowired
    private RegionService regionService;
    @Autowired
    private UserPService userPService;
    @Autowired
    private SalesService salesService;
    @Autowired
    private TimeRService timeRService;
    @RequestMapping("/mrInfo")
    public String findOne(HttpServletRequest req){
        Total mr = totalService.findOne();
        req.setAttribute("mr",mr);
        return "mr1";
    }
    @RequestMapping("/mrInfo2")
    public String findAll(HttpServletRequest req){
        List<Region> allRegion = regionService.findAll();
        req.setAttribute("allRegion",allRegion);
        return "mr2";
    }

    @RequestMapping("/mrInfo3")
    public String findAll2(HttpServletRequest req){
        List<UserP> all3 = userPService.findAll();
        req.setAttribute("all3",all3);
        return "mr3";
    }
    @RequestMapping("/mrInfo4")
    public String findAll3(HttpServletRequest req){
        List<SalesAll> all4 = salesService.findAll();
        req.setAttribute("all4",all4);
        return "mr4";
    }
    @RequestMapping("/mrInfo5")
    public String findAll4(HttpServletRequest req){
        List<TimeR> all1 = timeRService.findAll();
        req.setAttribute("all1",all1);
        return "mr5";
    }
}
