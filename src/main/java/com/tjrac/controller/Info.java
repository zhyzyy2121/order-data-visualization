package com.tjrac.controller;

import com.tjrac.entities.OneDay;
import com.tjrac.entities.Region;
import com.tjrac.entities.TimeR;
import com.tjrac.entities.UserP;
import com.tjrac.mapper.OneDayMapper;
import com.tjrac.service.RegionService;
import com.tjrac.service.TimeRService;
import com.tjrac.service.UserPService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Lenovo
 * @version 1.0
 * @description TODO
 * @date 11/8/2023 下午6:18
 */
@RestController
public class Info {
    @Autowired
    private OneDayMapper oneDayMapper;

    @Autowired
    private TimeRService timeRService;
    @Autowired
    private RegionService regionService;
    @Autowired
    private UserPService userPService;
    @RequestMapping("/info")
    public OneDay allQ(int st){
        // 调用业务层
        OneDay oneInfo = oneDayMapper.findOneInfo(st);
        oneInfo.toString();
        return oneInfo;
    }
    @RequestMapping("/info2")
    public List<TimeR> allQ(){
        // 调用业务层
        List<TimeR> all = timeRService.findAll();
        all.toString();
        return all;
    }

    @RequestMapping("/info3")
    public List<Region> all1(){
        // 调用业务层
        List<Region> all1 = regionService.findAll();
        all1.toString();
        return all1;
    }

    @RequestMapping("/info4")
    public List<UserP> all2(){
        // 调用业务层
        List<UserP> all2 = userPService.findAll();
        all2.toString();
        return all2;
    }
}
