package com.tjrac.controller;

import com.tjrac.entities.*;
import com.tjrac.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * @author Lenovo
 * @version 1.0
 * @description TODO
 * @date 11/8/2023 下午4:10
 */
@Controller
public class Show {
    @Autowired
    private AdminService adminService;
    @Autowired
    private OneDayService oneDayService;
    @Autowired
    private GoodInfoService goodInfoService;
    @Autowired
    private RegionService regionService;
    @Autowired
    private UserPService userPService;

    @Autowired
    private TotalService totalService;
    @Autowired
    private SalesService salesService;
    @Autowired
    private TimeRService timeRService;

    @PostMapping ("/show")
        public String echartsShow(HttpServletRequest req,String username,String password){
            OneDay oneInfo = oneDayService.findOneInfo(0);
            List<GoodInfo> all = goodInfoService.findAll();
            Total total = totalService.findOne();
            List<Region> allR = regionService.findAll();
            List<UserP> userPs = userPService.findAll();
            List<SalesAll> sales = salesService.findAll();
            List<TimeR> allT = timeRService.findAll();
            req.setAttribute("oneInfo", oneInfo);
            req.setAttribute("all",all);
            req.setAttribute("total",total);
            req.setAttribute("allR",allR);
            req.setAttribute("userPs",userPs);
            req.setAttribute("sales",sales);
            req.setAttribute("allT",allT);
        return "echarts";


//        Admin admin = adminService.findUserByLogin(username, password);
//        if(admin != null && admin.getPassword().equals(password)){
//            System.out.println("登录成功！！！");
//            return "echarts";
//        }else {
//            System.out.println("登录失败");
//            return "test";
//        }


    }

//    @RequestMapping("/allQ")
//    public ResultData allQ(){
//        // 调用业务层
//        ResultData data = adminService.findUserByLogin();
//        return ResultData.ok(data);
//    }


}
