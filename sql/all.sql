# 建表脚本
# @author <a href="https://zhy648.club/">随缘而愈</a>


-- 创建库
create database if not exists tjrac;

-- 切换库
use tjrac;

-- 订单的区域分布表
create table region if not exists region
(
    count varchar(256) null,
    place varchar(256) null
);comment '订单的区域分布' collate = utf8mb4_unicode_ci;

INSERT INTO tjrac.region (count, place) VALUES ('5230', '上海');
INSERT INTO tjrac.region (count, place) VALUES ('4521', '天津');
INSERT INTO tjrac.region (count, place) VALUES ('6052', '北京');
INSERT INTO tjrac.region (count, place) VALUES ('2341', '湖南省');
INSERT INTO tjrac.region (count, place) VALUES ('2341', '海南省');
INSERT INTO tjrac.region (count, place) VALUES ('3421', '广东省');
INSERT INTO tjrac.region (count, place) VALUES ('2312', '江西省');
INSERT INTO tjrac.region (count, place) VALUES ('3510', '浙江省');


-- Ⅰ类订单的区域分布表
create table regionOne if not exists regionOne
(
    userNum   varchar(256) null,
    userPlace varchar(256) null
);comment 'Ⅰ类订单的区域分布' collate = utf8mb4_unicode_ci;

INSERT INTO tjrac.userp (userNum, userPlace) VALUES ('4352', '上海');
INSERT INTO tjrac.userp (userNum, userPlace) VALUES ('3451', '天津');
INSERT INTO tjrac.userp (userNum, userPlace) VALUES ('5341', '北京');
INSERT INTO tjrac.userp (userNum, userPlace) VALUES ('2352', '湖南省');
INSERT INTO tjrac.userp (userNum, userPlace) VALUES ('3333', '海南省');
INSERT INTO tjrac.userp (userNum, userPlace) VALUES ('2345', '广东省');
INSERT INTO tjrac.userp (userNum, userPlace) VALUES ('2345', '江西省');
INSERT INTO tjrac.userp (userNum, userPlace) VALUES ('3456', '浙江省');
INSERT INTO tjrac.userp (userNum, userPlace) VALUES ('2006', '陕西省');
INSERT INTO tjrac.userp (userNum, userPlace) VALUES ('1220', '山西省');


-- 时间分布趋势表
create table good_time if not exists good_time
(
    timer varchar(256) null,
    count int          null
);comment '时间分布趋势' collate = utf8mb4_unicode_ci;

INSERT INTO tjrac.good_time (timer, count) VALUES ('6:00~12:00', 5002);
INSERT INTO tjrac.good_time (timer, count) VALUES ('12:00~18:00', 5203);
INSERT INTO tjrac.good_time (timer, count) VALUES ('18:00~24:00', 6402);
INSERT INTO tjrac.good_time (timer, count) VALUES ('other', 3081);

-- 销售额统计表 
create table sales if not exists sales
(
    month          varchar(256) null,
    yearExpect     varchar(256) null,
    quarterExpect  varchar(256) null,
    monthExpect    varchar(256) null,
    weekExpect     varchar(256) null,
    yearReality    varchar(256) null,
    quarterReality varchar(256) null,
    monthReality   varchar(256) null,
    weekReality    varchar(256) null
);comment '销售额统计' collate = utf8mb4_unicode_ci;

INSERT INTO tjrac.sales (month, yearExpect, quarterExpect, monthExpect, weekExpect, yearReality, quarterReality, monthReality, weekReality) VALUES ('1月', '24', null, null, null, '40', null, null, null);
INSERT INTO tjrac.sales (month, yearExpect, quarterExpect, monthExpect, weekExpect, yearReality, quarterReality, monthReality, weekReality) VALUES ('2月', '40', null, null, null, '64', null, null, null);
INSERT INTO tjrac.sales (month, yearExpect, quarterExpect, monthExpect, weekExpect, yearReality, quarterReality, monthReality, weekReality) VALUES ('3月', '101', null, null, null, '191', null, null, null);
INSERT INTO tjrac.sales (month, yearExpect, quarterExpect, monthExpect, weekExpect, yearReality, quarterReality, monthReality, weekReality) VALUES ('4月', '134', null, null, null, '324', null, null, null);
INSERT INTO tjrac.sales (month, yearExpect, quarterExpect, monthExpect, weekExpect, yearReality, quarterReality, monthReality, weekReality) VALUES ('5月', '90', null, null, null, '290', null, null, null);
INSERT INTO tjrac.sales (month, yearExpect, quarterExpect, monthExpect, weekExpect, yearReality, quarterReality, monthReality, weekReality) VALUES ('6月', '230', null, null, null, '330', null, null, null);
INSERT INTO tjrac.sales (month, yearExpect, quarterExpect, monthExpect, weekExpect, yearReality, quarterReality, monthReality, weekReality) VALUES ('7月', '210', null, null, null, '310', null, null, null);
INSERT INTO tjrac.sales (month, yearExpect, quarterExpect, monthExpect, weekExpect, yearReality, quarterReality, monthReality, weekReality) VALUES ('8月', '230', null, null, null, '213', null, null, null);
INSERT INTO tjrac.sales (month, yearExpect, quarterExpect, monthExpect, weekExpect, yearReality, quarterReality, monthReality, weekReality) VALUES ('9月', '120', null, null, null, '180', null, null, null);
INSERT INTO tjrac.sales (month, yearExpect, quarterExpect, monthExpect, weekExpect, yearReality, quarterReality, monthReality, weekReality) VALUES ('10月', '230', null, null, null, '200', null, null, null);
INSERT INTO tjrac.sales (month, yearExpect, quarterExpect, monthExpect, weekExpect, yearReality, quarterReality, monthReality, weekReality) VALUES ('11月', '210', null, null, null, '180', null, null, null);
INSERT INTO tjrac.sales (month, yearExpect, quarterExpect, monthExpect, weekExpect, yearReality, quarterReality, monthReality, weekReality) VALUES ('12月', '120', null, null, null, '99', null, null, null);


